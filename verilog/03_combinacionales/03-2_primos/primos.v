// Diseño:      primos
// Archivo:     primos.v
// Descripción: Detector de números primos
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       09/11/2018

/*
   Lección 3-2: Detector de números primos de 4 bits.

   Este diseño consiste en un detector de números primos de 4 bits. Cuando los
   bits de entrada corresponden a la representación en binario natural
   (base 2) de un número primo, la salida se activa a 1, en otro caso
   permanece a 0.

   En esta lección se practican, entre otros, los siguientes conceptos:

     - Señales de múltiples bits (vectores)
     - Generación de patrones de test mediante un contador
*/

`timescale 1ns / 1ps

// Detector de números primos

module primos (
    /* Las señales en Verilog pueden ser de múltiples bits. El rango de bits de
     * la señal se define tras el tipo de señal como [m:n], por ejemplo "[1:7]"
     * o "[7:0]". Lo más habitual es definir los rangos de índice mayor a índice
     * menor, ya que es la forma más conveniente cuando la señal representa un
     * número binario. Por ejemplo:
     *   reg [7:0] x;
     * A una señal multibit se le puede asignar un valor numérico que será
     * almacenado en binario en la señal. Se puede obtener el valor de un bit
     * concreto de la señal empleando el operador índice como "x[i]". Ejemplo:
     *   assign a = x[3];
     * También es posible obtener un rango de bits con el mismo operador.
     * Ejemplo:
     *   wire [3:0] a;
     *   wire [7:0] x;
     *   assign x = 184;
     *   assign a = x[7:4];
     */
    input wire [3:0] x, // dato de entrada (4 bits)
    output reg z        // salida. 0-no primo, 1-primo
    );

    /* Usamos un procedimiento para describir el circuito */
    always @* begin
        /* La sentencia "case" permite realizar operaciones diferentes en
         * función del valor de una variable o expresión. Para nuestro caso es
         * ideal ya que el valor de la salida dependerá sólo del valor de "x".*/
        case(x)         /* expresión a probar */
        2:  z = 1;      /* acción en caso de valor "2" */
        3:  z = 1;      /* etc. */
        5:  z = 1;
        7:  z = 1;
        11: z = 1;
        13: z = 1;
        default: z = 0; /* acción en cualquier otro caso */
        endcase
    end

endmodule // primos

/*
   La lección continúa en el archivo primos_tb.v
*/
