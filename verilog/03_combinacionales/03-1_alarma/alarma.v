// Diseño:      alarma
// Archivo:     alarma.v
// Descripción: Alarma sencilla para un automovil
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       08/11/2018

/*
   Lección 3-1: Ejemplos combinacionales. Alarma de automovil.

   Este archivo contiene una posible solución al siguiente problema:

   Se quiere diseñar un sistema de alarma para un coche que impida que el
   conductor abandone el coche con el motor encendido o con las luces
   encendidas. El coche tiene sensores para la puerta del conductor, el motor
   y las luces que indican el estado de cada elemento:
     * Sensor del motor: 0-apagado, 1-encendido.
     * Sensor de puerta: 0-cerrada, 1-abierta.
     * Sensor de luces: 0-apagadas, 1-encendidas.
   La sirena se controla mediante una señal digital, y suena cuando la señal
   vale uno.
   Diseñe un circuito que haga sonar la alarma cuando:
     * El motor esté encendido y la puerta abierta.
     * El motor está apagado, las luces encendidas y la puerta abierta.

   El archivo alarma_tb.v contiene el banco de pruebas para el test de este
   diseño.

   En esta lección se practican, entre otros, los siguientes conceptos:

     - Lista de sensibilidad "@*"
*/

`timescale 1ns / 1ps

/* Definimos el módulo con una entrada para cada elemento que puede afectar
 * al estado de la alarma */
module alarma(
    input wire puerta,   // sensor de la puerta. 0-cerrada, 1-abierta
    input wire motor ,   // estado del motor. 0-apagado, 1-encendido
    input wire luces ,   // estado de las luces. 0-apagadas, 1-encendidas
    output reg sirena    // estado de la alarma. 0-no suena, 1-suena
                         /* Declaramos la salida como "reg" ya que se va a
                          * generar en un bloque 'always'. */
    );

    /* La función se describe de forma procedimental en un bloque "always".
     * La construcción "@(*)" indica que se incluyan todas las variables
     * presentes en el bloque en la lista de sensibilidad con objeto de
     * obtener una implementación combinacional. De esta forma se evitan
     * errores en caso de olvidar incluir alguna variable. En este caso es
     * equivalente a "@(p,m,l)". */
     always @(*)
     	if (motor == 1 && puerta == 1)
     		sirena = 1;  // activamos cuando se cumple la 1ª condición
     	else if (motor == 0 && luces == 1 && puerta == 1)
     		sirena = 1;  // también cuando se cumple la 2ª condición
     	else
     		sirena = 0;  // en cualquier otro caso no suena la alarma

endmodule // alarma

/*
   La lección continúa en el archivo alarma_tb.v
*/
