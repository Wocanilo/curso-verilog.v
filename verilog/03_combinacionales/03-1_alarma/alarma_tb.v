// Diseño:      alarma
// Archivo:     alarma_tb.v
// Descripción: Alarma sencilla para un automovil
// Autor:       Jorge Juan-Chico <jjchico@gmail.com>
// Fecha:       08/11/2018

/*
   Lección 3-1: Ejemplos combinacionales. Alarma de automovil.

   Este archivo contiene un banco de pruebas para el módulo alarma.
*/

`timescale 1ns / 1ps

module test ();

    // Entradas
    reg puerta, motor, luces;

    // Salidas
    wire sirena;

    // Instancia de la unidad bajo test (UUT)
    /* Es una buena práctica usar una línea para conectar cada señal. De
     * esta forma es más fácil ver si se han conectado todas las señales y,
     * también, hacer futuras modificaciones en el diseño, como quitar o
     * añadir señales a los módulos. */
    alarma uut (
        .puerta(puerta),
        .motor(motor),
        .luces(luces),
        .sirena(sirena)
        );

    /* El siguiente proceso "initial" inicia los patrones de test e incluye
     * las directivas de simulación */
    initial begin
        // Iniciamos las entradas
        puerta = 0;     // cerrada
        motor = 0;      // apagado
        luces = 0;      // apagadas

        // Genera formas de onda
        $dumpfile("alarma_tb.vcd");
        $dumpvars(0,test);

        // Pruebas del circuito
        /* Cambiamos las señales de forma que el circuito debe activar la
         * salida (suena la alarma). Comprobamos los dos casos de interés:
         *   - pueta abierta con el motor encendido.
         *   - puerta abierta con motor apagado y luces encendidas. */

        // Puerta abierta y motor encendido
        #10 motor = 1;      // arrancamos el motor
        #10 puerta = 1;     // abrimos la puerta
                            // aquí debe sonar la alarma
        #10 puerta = 0;     // cerramos la puerta
                            // aquí debe dejar de sonar la alarma
        #10 motor = 0;      // apagamos el motor
        #10 luces = 1;      // encendemos las luces
        #10 puerta = 1;     // abrimos la puerta
                            // aquí debe sonar la alarma
        #10 luces = 0;      // apagamos las luces
                            // aquí debe dejar de sonar la alarma
        #10 puerta = 0;     // cerramos la puerta

        // Finalizamos la simulación
        #20 $finish;
    end

endmodule // test

/*
   EJERCICIIO

   1. Compila la lección con:

      $ iverilog alarma.v alarma_tb.v

   2. Simula el diseño con:

      $ vvp a.out

   3. Observa la formas de onda resultado de la simulación con:

      $ gtkwave alarma_tb.vcd &

      Para interpretar mejor el resultado, selecciona las señales del módulo
      "uut" (unidad bajo test) en el orden: motor, puerta, luces, sirena.
      Comprueba que la salida "sirena" tiene un valor correcto que corresponda
      con el enunciado del problema.

   4. Esta alarma puede llegar a ser molesta. Añada una entrada de control de
      activación de la alarma (puede llamarla "control") de forma que si
      control=0 la alarma permance desactivada y con control=1 funciona
      normalmente. Modifique el banco de pruebas para probar esta nueva
      funcionalidad y simule y corrija el circuito hasta que opere
      correctamente.
*/
